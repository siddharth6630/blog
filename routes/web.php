<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('blogs', 'UserProfile@blogs')->name('blogs');
Route::get('fullBlog/{id}', 'UserProfile@fullBlog')->name('fullBlog');

Route::get('backend/login', 'UserProfile@login')->name('login');

Route::post('backend/loginSubmit', 'UserProfile@loginSubmit')->name('loginSubmit');