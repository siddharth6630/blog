<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

/* Add a gray background color with some padding */
body {
  font-family: Arial;
  padding: 20px;
  background: #f1f1f1;
}

/* Header/Blog Title */
.header {
  padding: 30px;
  font-size: 40px;
  text-align: center;
  background: white;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #aaa;
  width: 100%;
  padding: 20px;
}

/* Add a card effect for articles */
.card {
   background-color: white;
   padding: 20px;
   margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background: #ddd;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}
</style>
</head>
<body>

<div class="row">
  <div class="leftcolumn">
	
	@foreach ($blogs as $key => $blog)
		<div class="card">
		  <h2>{{ $blog->blog_name }}</h2>
		  <h5>{{ $blog->blog_name }}, {{ date('d/m/Y', strtotime($blog->created_date)) }}</h5>
		  <div class="fakeimg" style="height:200px;">Image {{$key+1}}</div>
		  
		  <p>{{ str_limit($blog->blog_test, 255) }}</p><a href="{{route('fullBlog', $blog->id)}}">Click Here...</a>
		</div>
	@endforeach
  </div>
</div>
</body>
</html>
