<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserProfile extends Controller
{
	public function blogs()
	{
		$blogs = \DB::table('blogs')->get();
		
		return view('blogs')->with(['blogs' => $blogs]);
	}
	
	public function fullBlog($id = 0) 
	{
		$fullblogs = \DB::table('blogs')->where('id', $id)->first();
		return view('fullblog')->with(['fullblog' => $fullblogs]);
	}
	
    public function login()
	{
		return view('backend/login');
	}
	
	public function loginSubmit(Request $request)
	{
		$product = $this->validate(request(), [
			'name' => 'required',
			'password' => 'required|alphaNum|min:3'
        ]);
		
        if(!empty(request()->name) && !empty(request()->password))
		{
			$name = request()->name;
			$password = request()->password;
			
			if(\DB::table('blog_users')->where('name', $name)->exists())
			{
				$User = \DB::table('blog_users')->where('name', $name)->first();
				$sessionUserId = $request->session()->put('UserId', $User->id);
				$userRole = $User->role;
				
				$UserArray =[ 'sessionUserId' => $sessionUserId, 'UserRole' => $userRole];
				
				return view('backend/add_edit_blogs')->with(['UserArray' => $UserArray]);
			} else {
				 return redirect()->back()->with('error','You have no permission!');
			}
			
			//echo "<pre>";print_r($users);die;	
			
			
			echo "HI";die;
			return view('backend/login');
		} else
		{
			return view('backend/login');
		}
		

	}
}
